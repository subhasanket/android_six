package com.example.application_six

import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.io.File

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val filePath:String = "/system/fonts"
        val file = File(filePath)
        val listFilePath = file.listFiles()
        val list = ArrayList<String>()
        for (i in listFilePath){
            list.add(i.name.substring(0,i.name.length-4))
        }

        val adapter_var = ArrayAdapter(this,android.R.layout.simple_list_item_1,list)
        list_view.adapter = adapter_var

        list_view.onItemClickListener =
            AdapterView.OnItemClickListener { parent, view, position, id ->
                val itemValue = list_view.getItemAtPosition(position) as String

                tv_text.setTypeface(Typeface.createFromFile(listFilePath[position].toString()))
            }
    }
}
